#!/bin/bash

declare -a indices

export indices=(
  camoproxy
  chef
  consul
  fluentd
  gitaly
  gcs
  gcp-events
  gke
  gke-audit
  gke-systemd
  jaeger
  kas
  mailroom
  monitoring
  pages
  postgres
  praefect
  pubsubbeat
  puma
  pvs
  rails
  redis
  registry
  runner
  sentry
  shell
  sidekiq
  system
  vault
  workhorse
)
